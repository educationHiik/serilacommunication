package hiik.communication.serial;

import com.fazecast.jSerialComm.SerialPort;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import static hiik.communication.serial.Button.*;

/**
 * @author vaganovdv
 *
 * Класс читает команду из консоли, на отправку sms
 * TODO: Нужно прикрутить сюда остальные команды на вкл/откл оборудования Assing: Max Shevchenkov
 * TODO: Сменить имя проекта - предлагается TC65CommunicationManager
 */
public class ConsoleReader implements Runnable {

    private SerialPortManager manager = new SerialPortManager();
    private Scanner sc = new Scanner(System.in);

    private volatile int index;

    /**
     * Метод пишет в консоль приветствие и запускает менеджер(обработчик команд)
     * {@link SerialPortManager#start()}
     */
    public void start() {

        System.out.println("");
        System.out.println("-------------------------------------------------");
        System.out.println("Serial Communication Manager version 0.0.2");
        System.out.println("-------------------------------------------------");
        manager.start();
    }

    /**
     * Метод обрабатывает строку (команду) и запускает соответствующие методы оператором switch
     *
     * @param str - параметр - строка содержащая команду
     *
     * {@link #sms()}
     * {@link #exit()}
     * {@link #select()}
     * {@link #send()}
     **/
    private void processString(String str) {

        if (str != null && !str.isEmpty()) {
            switch (str.trim()) {
                case "exit":
                    exit();
                    break;
                case "select":
                    select();
                    break;
                case "send":
                    send();
                    break;
                case "sms":
                    sms();
                    break;
            }
        }
    }

    /**
     * Метод инициирует выход из приложения
     **/
    private void exit() {

        System.out.println("Exiting....");
        System.exit(0);
    }

    /**
     * Метод проверяет наличие текущего порта {@link SerialPortManager#getCurrentSerialPort()}
     * Читает с консоли команду и отправляет ее на текущий порт {@link SerialPortManager#writeString(SerialPort, String)}
     **/
    private void send() {

        if (manager.getCurrentSerialPort() != null) {
            System.out.print("enter command:");
            String smsText = sc.nextLine();
            manager.writeString(manager.getCurrentSerialPort(), smsText);
        } else {
            System.out.println("select the port to work with");
        }
    }

    /**
     * Метод получает список портов {@link SerialPortManager#listPorts()}
     * Пользователь выбирает из списка порт, этот порт устанавливается в качестве текущего
     * {@link SerialPortManager#setCurrentSerialPort(SerialPort)}
     * **/
    private void select() {

        int index = 0;
        if (!manager.getSerialPorts().isEmpty()) {

            System.out.println("Select port from list:");
            manager.listPorts();
            String selection = sc.nextLine();
            if (manager.getSerialPorts().containsKey(selection.trim())) {
                manager.setCurrentSerialPort(manager.getSerialPorts().get(selection.trim()));
                System.out.println("selection :" + manager.getCurrentSerialPort().getSystemPortName());
            } else {
                System.out.println("error: port [" + selection.trim() + "] not found");
            }
        } else {
            System.out.println("Ports not founds");
        }
    }

    /**
     * Инициатор логики в отдельном потоке
     * Переопределяет метод run интерфейса Runnable {@link Runnable#run()}
     * **/
    @Override
    public void run() {

        System.out.print("# ");

        while (sc.hasNextLine()) {

            String input = sc.nextLine();
            if (!input.isEmpty()) {
                this.processString(input);
            }
            System.out.print("# ");
        }
    }

    /**
     * Метод инициирует отпраку смс в контексте терминала
     *
     * Команды:
     *
     * atz - сброс настроек
     * at + cmgf=1 - инициализация терминала на отправку смс
     * at + сmgs - принимает номер телефона
     *
     * Примечание: Команды отправляются с переодичностью 400 мс
     * для создания необходимого буфера по времени между командами
     *
     * Более подробно ознакомиться с командами можно по адресу
     * http://papouch.com/data/user-content/old_eshop/files/GSM_TC65i/tc65-at-commands.pdf
     *
     * **/
    private void sms() {

        if (manager.getCurrentSerialPort() != null) {
            try {
                System.out.print("enter phone number:");
                String phone = sc.nextLine();

                System.out.print("enter sms text  number:");
                String text = sc.nextLine();

                manager.writeString(manager.getCurrentSerialPort(), "atz" + ENTER);
                Thread.sleep(400);

                manager.writeString(manager.getCurrentSerialPort(), "at+cmgf=1" + ENTER);
                Thread.sleep(400);

                manager.writeString(manager.getCurrentSerialPort(), "at+cmgs=" + PLUS + phone + ENTER);
                Thread.sleep(400);

                manager.writeString(manager.getCurrentSerialPort(), text + CTRLZ);
            } catch (InterruptedException ex) {
                Logger.getLogger(ConsoleReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("select the port to work with");
        }
    }
}
