package hiik.communication.serial;

/**
 * @author Max Shevchenkov
 * Вспомагательные кнопки в ASCII-коде
 */
public class Button {
    public static final char ENTER = 13;
    public static final char CTRLZ = 26;
    public static final char PLUS = 43;
}
