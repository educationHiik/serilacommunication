package hiik.communication.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

/**
 * @author vaganovdv
 * Класс - слушатель. Фиксирует событие завершения процесса записи в порт. 
 * Печатает в консоль строковое представление события
 */

public class SerialWriter implements SerialPortDataListener {

    /**
     * Метод инициирует слушателя событий записи
     * @return Настроенный слушатель порта
     * **/
    @Override
    public int getListeningEvents() {

        return SerialPort.LISTENING_EVENT_DATA_WRITTEN;
    }

    /**
     * Метод обработки ответа от терминала. Принимает ответ. Печатает в консоль
     * @param event - событие от слушателя
     * **/
    @Override
    public void serialEvent(SerialPortEvent event) {

        if (event.getEventType() == SerialPort.LISTENING_EVENT_DATA_WRITTEN) {
            System.out.println("Written  to comport [" + event.getSerialPort().toString() + "]");
        }
    }
}
