package hiik.communication.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

/**
 * @author vaganovdv
 * Класс - слушатель. Фиксирует событие при получении байтового массива. 
 * 
 */

public class SerialReader implements SerialPortDataListener {

    /**
     * Метод инициирует слушателя событий чтения
     * @return Настроенный слушатель порта
     * **/
    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    /**
     * Метод обработки ответа от терминала. Принимает ответ. Печатает в консоль
     * @param event - событие от слушателя
     * **/
    @Override
    public void serialEvent(SerialPortEvent event) {

        byte[] newData = event.getReceivedData();
        System.out.print("[" + event.getSerialPort().getSystemPortName() + "] received " + newData.length + " bytes ==> ");
        for (int i = 0; i < newData.length; ++i) {
            System.out.print((char) newData[i]);
        }
        System.out.println("\n");
    }
}
