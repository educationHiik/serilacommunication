package hiik.communication.serial;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author vaganovdv
 */

/**
 * Метод создает пулл потоков. Запускает приложение в отдельном потоке
 * **/
public class SerialCommunication {
    
    public static void main(String[] args) {
       
        ExecutorService executor = Executors.newFixedThreadPool(1);

        ConsoleReader consoleReader = new ConsoleReader();
        consoleReader.start();

        executor.submit(consoleReader);

        SerialWriter sw = new SerialWriter();
        sw.getListeningEvents();
    }
}
