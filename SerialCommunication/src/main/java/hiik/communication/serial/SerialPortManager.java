package hiik.communication.serial;

import com.fazecast.jSerialComm.SerialPort;

import static com.fazecast.jSerialComm.SerialPort.NO_PARITY;
import static com.fazecast.jSerialComm.SerialPort.FLOW_CONTROL_DISABLED;
import static hiik.communication.serial.Button.ENTER;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author vaganovdv
 * Менеджер работы с терминалом.
 * 
 */
public class SerialPortManager {

    private Map<String, SerialPort> serialPorts = new LinkedHashMap<>();
    private int index;

    private SerialPort currentSerialPort;

    /**
     * Метод инициализирует COMM-порты.
     * Получает список портов {@link #listPorts()}, устанавливает дефолтные значения портов {@link #setDefaultParameters(SerialPort)},
     * кладет в карту где ключ - имя порта , значение - объект порта
     * {@link #serialPorts}
     * **/
    public void start() {

        System.out.println("Starting ...");

        SerialPort[] portList = SerialPort.getCommPorts();

        if (portList != null && portList.length > 0) {
            int index = 0;
            for (SerialPort serialPort : portList) {
                index++;
                serialPorts.put(serialPort.getSystemPortName(), serialPort);
                if (openPort(serialPort, index)) {
                    this.setDefaultParameters(serialPort);
                }
                addListener(serialPort);
            }
            System.out.println("List of ports:");
            this.listPorts();
        } else {
            System.out.println("Comm ports not found");
        }
    }

    /**
     * Метод получения списка портов
     * **/
    public void listPorts() {

        if (!this.serialPorts.isEmpty()) {
            index = 1;
            serialPorts.forEach((name, port) -> {
                index++;
                StringBuilder sb = new StringBuilder();
                sb.append(String.format("%-5s", "[" + index + "]"));
                sb.append(String.format("%-20s", name));
                sb.append(String.format("%-12s", "status: " + port.isOpen()));
                System.out.println(sb.toString());
            });
        } else {
            System.out.println("error: no ports found");
        }
    }

    /**
     *  Метод печатает в консоль информацию о параметрах порта
     *  @param port - порт
     *  @return Строковая вид объекта порта
     *
     *  //TODO если метод не нужен в работе приложения нужно выпилить
     * **/
    public String printParameters(SerialPort port) {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("%-25s", "port name:", port.getDescriptivePortName()));
        sb.append("\n");

        sb.append(String.format("%-25s", "description:", port.getPortDescription()));
        sb.append("\n");

        sb.append(String.format("%-25s", "is open?:", port.isOpen()));
        sb.append("\n");

        sb.append(String.format("%-25s", "baud rate:", port.getBaudRate()));
        sb.append("\n");

        sb.append(String.format("%-25s", "flow control:", port.getFlowControlSettings()));
        sb.append("\n");

        sb.append(String.format("%-25s", "data bits:", port.getNumDataBits()));
        sb.append("\n");

        sb.append(String.format("%-25s", "parity:", port.getParity()));
        sb.append("\n");

        sb.append(String.format("%-25s", "stop bits:", port.getNumStopBits()));
        return sb.toString();
    }

    /**
     *  Метод открывает порт для коммуникации
     *  @param port - порт который необходимо открывать
     *  @param  index - номер порта в списке
     *
     *  @return Результат открытия порта
     * **/
    public boolean openPort(SerialPort port, int index) {

        boolean result = false;
        if (port.openPort()) {
            result = true;
            System.out.println("[" + index + "] port  [" + port.getSystemPortName() + "] opened!");
        } else {
            System.out.println("error: failed to open port [" + port.getSystemPortName() + "]");
        }

        return result;
    }

    /** Метод установки дефолтных значений порту
     *  @param port - порт для настройки
     * **/
    public void setDefaultParameters(SerialPort port) {
        port.setBaudRate(115200);
        port.setParity(NO_PARITY);
        port.setFlowControl(FLOW_CONTROL_DISABLED);
        port.setNumDataBits(8);
        port.setNumStopBits(1);
        port.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 1000, 0);
    }

    /**
     * Метод добавляет слушаетелей чтения/записи в(из) порта
     * **/
    public void addListener(SerialPort serialPort) {

        if (serialPort != null) {
            serialPort.addDataListener(new SerialReader());
            serialPort.addDataListener(new SerialWriter());
        }
    }


    public Map<String, SerialPort> getSerialPorts() {
        return serialPorts;
    }

    /**
     * Метод инициализирует выход из приложения со статусом 0
     * **/
    public void stop() {

        System.out.println("Exiting...");
        System.exit(0);
    }

    /**
     * Метод передачи команды на TC-65.
     * Берет из карты порт по ключу. Читает байты из строки команды
     * Передает на терминал
     * Печатает в консоль состояние передачи
     *
     * @param serialPort - порт для передачи
     * @param commandToSend  - команда для отпрвки на порт
     * **/
    public void writeString(SerialPort serialPort, String commandToSend) {

        if (serialPorts.containsKey(serialPort.getSystemPortName())) {
            commandToSend = commandToSend + ENTER;
            byte[] buffer = commandToSend.getBytes();
            serialPort.writeBytes(buffer, buffer.length);

            System.out.println("[" + serialPort.getSystemPortName() + "status:" + serialPort.isOpen() + "] ==> " + commandToSend);
        } else {
            System.out.println("Error: comport [" + serialPort.getSystemPortName() + "] not found");
        }
    }

    public SerialPort getCurrentSerialPort() {

        return currentSerialPort;
    }

    public void setCurrentSerialPort(SerialPort currentSerialPort) {

        this.currentSerialPort = currentSerialPort;
    }
}
